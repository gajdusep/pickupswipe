package com.example.pickupswipe;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageHandler {

    private static final ExecutorService DELETE_SERVICE = Executors.newSingleThreadExecutor();

    public static void deleteImage(String path) {
        if (path == null) return;
        final File file = new File(path);
        DELETE_SERVICE.submit(() -> {
            boolean resultDelete = file.delete();
            Log.d("file", "image deleted: " + resultDelete);
        });
    }

    public static void loadImageToImageView(Context context, String path, ImageView imageView, int width, int height) {
        Glide.with(context).asBitmap().load(path)
                .apply(new RequestOptions().override(width, height))
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource,
                                                @Nullable Transition<? super Bitmap> transition) {
                        imageView.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }

    public static void loadImageToImageView(Context context, String path, ImageView imageView) {
        Glide.with(context).asBitmap().load(path)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource,
                                                @Nullable Transition<? super Bitmap> transition) {
                        imageView.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }

    public static void logAllImages(Context context) {
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File[] files = directory.listFiles();
        Log.d("Images", "Size: " + files.length);
        for (File file : files) {
            Log.d("Images", "FileName: " + file.getName());
        }
    }

    public static String saveImage(Context context, Bitmap bitmap) {
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        String newImageFileName = "" + System.currentTimeMillis() + ".jpg";
        File imagePath = new File(directory, newImageFileName);
        FileOutputStream outputStream = null;
        try {
            Log.d("file", "compressing image");
            outputStream = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);
            Log.d("file", "image compressed");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.d("file", "closing stream");
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return imagePath.getAbsolutePath();
    }
}
