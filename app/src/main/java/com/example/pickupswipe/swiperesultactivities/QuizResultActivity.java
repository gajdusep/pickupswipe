package com.example.pickupswipe.swiperesultactivities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.pickupswipe.R;
import com.example.pickupswipe.results.QuizResult;
import com.example.pickupswipe.results.ResultFactory;

import org.json.JSONException;
import org.json.JSONObject;

public class QuizResultActivity extends BaseResultActivity {
    QuizResult quizResult;
    TextView questionTV;
    TextView[] answers;
    Button okCloseButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);
        setSwipeResultTextView();

        questionTV = findViewById(R.id.questionTV);
        answers = new TextView[]{
                findViewById(R.id.quizAnswer1TV), findViewById(R.id.quizAnswer2TV),
                findViewById(R.id.quizAnswer3TV), findViewById(R.id.quizAnswer4TV)
        };
        okCloseButton = findViewById(R.id.okCloseButton);
        okCloseButton.setVisibility(View.INVISIBLE);

        try {
            prepareQuestion();
        } catch (JSONException e) {
            Log.d("json error", e.toString());
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void prepareQuestion() throws JSONException {
        ResultFactory factory = new ResultFactory();
        JSONObject o = new JSONObject(this.swipeResultJSON);
        quizResult = factory.getQuizResultFromJSONObject(o);

        questionTV.setText(quizResult.question);
        for (TextView tv : answers) {
            tv.setVisibility(View.GONE);
        }

        String[] letters = new String[]{"A", "B", "C", "D"};
        for (int i = 0; i < quizResult.answers.size(); i++) {
            String s = letters[i] + ") " + quizResult.answers.get(i);
            answers[i].setText(s);
            answers[i].setVisibility(View.VISIBLE);
            final int j = i;

            answers[i].setOnClickListener(v -> {
                TextView answerTV = answers[j];
                if (quizResult.isCorrect.get(j)) {
                    answerTV.setBackgroundResource(R.color.lightcorrect);
                    answerTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_check_circle_24, 0);
                    okCloseButton.setVisibility(View.VISIBLE);
                } else {
                    answerTV.setBackgroundResource(R.color.lightincorrect);
                    answerTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_highlight_off_24, 0);
                }
                answerTV.setClickable(false);
            });
        }
    }

    public void onOKClick(View v) {
        finish();
    }
}