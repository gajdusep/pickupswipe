package com.example.pickupswipe.swiperesultactivities;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pickupswipe.R;
import com.example.pickupswipe.SwipeActivity;
import com.example.pickupswipe.entities.AppRoomDatabase;
import com.example.pickupswipe.enums.ResultType;
import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;

public class BaseResultActivity extends AppCompatActivity {
    protected int profileId;
    protected SwipeResultActivityTypeDetailed swipeResultDetailed;
    protected String swipeResult;
    protected ResultType swipeResultType;
    protected String swipeResultJSON;
    protected AppRoomDatabase appRoomDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        profileId = extras.getInt(SwipeActivity.PROFILE_ID_EXTRA);
        swipeResult = extras.getString(SwipeActivity.SWIPE_RESULT);
        swipeResultType = ResultType.fromString(swipeResult);
        swipeResultDetailed = SwipeResultActivityTypeDetailed.fromString(extras.getString(SwipeActivity.SWIPE_RESULT_TYPE));
        swipeResultJSON = extras.getString(SwipeActivity.SWIPE_RESULT_JSON);
    }

    protected void setSwipeResultTextView() {
        TextView resultTextView = findViewById(R.id.swipeResultText);
        switch (swipeResultType) {

            case Like:
                resultTextView.setText(R.string.its_a_match);
                break;
            case Dislike:
                resultTextView.setText(R.string.its_not_a_match_however);
                break;
            case Superlike:
                resultTextView.setText(R.string.its_a_superlike);
                break;
        }
    }
}
