package com.example.pickupswipe.swiperesultactivities;

import android.os.Bundle;
import android.text.InputType;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.pickupswipe.App;
import com.example.pickupswipe.Constants;
import com.example.pickupswipe.views.HideKeyboardEditText;
import com.example.pickupswipe.ImageHandler;
import com.example.pickupswipe.R;
import com.example.pickupswipe.entities.MessageEntity;
import com.example.pickupswipe.enums.InteractionType;
import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class CustomResultActivity extends BaseResultActivity {
    LinearLayout resultLLToFill;
    ArrayList<View> allViews;
    String photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_result);
        setSwipeResultTextView();
        appRoomDatabase = ((App) getApplication()).appRoomDatabase;

        try {
            prepareActivity();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void prepareActivity() throws JSONException {
        allViews = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(swipeResultJSON);
        JSONArray jsonArray = jsonObject.getJSONArray(Constants.SwipeResultActionsArrayKey);

        Button saveButton = findViewById(R.id.saveButton);
        if (swipeResultDetailed == SwipeResultActivityTypeDetailed.SimpleText) {
            saveButton.setText(R.string.ok_text);
        }
        resultLLToFill = findViewById(R.id.resultLLToFill);

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject o = (JSONObject) jsonArray.get(i);
            String typeString = o.getString(Constants.InteractionTypeKey);
            InteractionType interactionType = InteractionType.fromString(typeString);
            switch (interactionType) {

                case TextViewType:
                    TextView first = new TextView(new ContextThemeWrapper(this, R.style.simple_result_text));
                    first.setText(o.getString(Constants.ActionTextKey));
                    first.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    resultLLToFill.addView(first);
                    allViews.add(first);
                    break;
                case InputTextType:
                    EditText et1 = new HideKeyboardEditText(this);
                    et1.setSingleLine(false);
                    et1.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                    resultLLToFill.addView(et1);
                    allViews.add(et1);
                    break;
                case InputPhoneType:
                    EditText et2 = new HideKeyboardEditText(this);
                    et2.setInputType(InputType.TYPE_CLASS_PHONE);
                    resultLLToFill.addView(et2);
                    allViews.add(et2);
                    break;
                case PhotoType:
                    ImageView imageView = new ImageView(new ContextThemeWrapper(this, R.style.result_image), null, 0);
                    String path = o.getString(Constants.PhotoPathKey);
                    photoPath = path;
                    ImageHandler.loadImageToImageView(this, path, imageView);
                    resultLLToFill.addView(imageView);
                    allViews.add(imageView);
                    break;
            }
        }
    }

    public void saveMessage(View v) throws JSONException {
        MessageEntity m = new MessageEntity();

        JSONObject answerJSON = new JSONObject();
        JSONArray answerArray = new JSONArray();
        for (View view : allViews) {
            JSONObject viewJSON = new JSONObject();
            if (view instanceof EditText) {
                viewJSON.put(Constants.ItemTypeKey, InteractionType.TextViewType.toString());
                viewJSON.put(Constants.ThemString, ((EditText) view).getText().toString());
            } else if (view instanceof TextView) {
                viewJSON.put(Constants.ItemTypeKey, InteractionType.TextViewType.toString());
                viewJSON.put(Constants.YouString, ((TextView) view).getText().toString());
            } else if (view instanceof ImageView) {
                viewJSON.put(Constants.ItemTypeKey, InteractionType.PhotoType.toString());
                viewJSON.put(Constants.YouString, photoPath);
            }
            answerArray.put(viewJSON);
        }
        answerJSON.put(Constants.AnswerArrayString, answerArray);
        m.messageJSON = answerJSON.toString();
        m.resultTypeString = swipeResult;
        m.profileID = profileId;
        m.datetime = System.currentTimeMillis();

        Executor myExecutor = Executors.newSingleThreadExecutor();
        myExecutor.execute(() -> {
            appRoomDatabase.messageDao().insertAll(m);
        });
        finish();
    }
}
