package com.example.pickupswipe.results;

import com.example.pickupswipe.Constants;
import com.example.pickupswipe.enums.InteractionType;
import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SimpleTextResult extends CustomResult {
    public String simpleText;

    public SimpleTextResult(String simpleText) {
        super();
        this.simpleText = simpleText;
        activityResultDetailed = SwipeResultActivityTypeDetailed.SimpleText.toString();
    }

    @Override
    public String toJSONString() throws JSONException {
        JSONArray actionsArray = new JSONArray();
        JSONObject simpleTextJSONObject = new JSONObject();
        simpleTextJSONObject.put(Constants.InteractionTypeKey, InteractionType.TextViewType);
        simpleTextJSONObject.put(Constants.ActionTextKey, simpleText);
        actionsArray.put(simpleTextJSONObject);

        JSONObject o = new JSONObject();
        addActionsArrayIntoObject(o, actionsArray);
        addActivityResultStringsToJSONObject(o);
        return o.toString();
    }
}
