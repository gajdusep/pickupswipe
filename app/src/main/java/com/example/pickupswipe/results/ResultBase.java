package com.example.pickupswipe.results;

import com.example.pickupswipe.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class ResultBase {
    public String activityResultDetailed;
    public String activityResultSimple;
    public ResultBase() {
    }

    public void addActivityResultStringsToJSONObject(JSONObject o) throws JSONException {
        o.put(Constants.SwipeResultActivityTypeDetailedStringKey, activityResultDetailed);
        o.put(Constants.SwipeResultActivityTypeSimpleStringKey, activityResultSimple);
    }

    public void getActivityResultStringsFromJSONObject(JSONObject o) throws JSONException {
        activityResultDetailed = o.getString(Constants.SwipeResultActivityTypeDetailedStringKey);
    }

    abstract public String toJSONString() throws JSONException;
}
