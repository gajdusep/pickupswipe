package com.example.pickupswipe.results;

import com.example.pickupswipe.Constants;
import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;
import com.example.pickupswipe.enums.SwipeResultActivityTypeSimple;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QuizResult extends ResultBase {
    public String question;
    public ArrayList<String> answers;
    public ArrayList<Boolean> isCorrect;

    public QuizResult(String question, ArrayList<String> answers, ArrayList<Boolean> isCorrect) {
        super();
        activityResultSimple = SwipeResultActivityTypeSimple.Quiz.toString();
        activityResultDetailed = SwipeResultActivityTypeDetailed.Quiz.toString();

        this.question = question;
        this.answers = answers;
        this.isCorrect = isCorrect;
    }

    @Override
    public String toJSONString() throws JSONException {

        JSONArray answersArray = new JSONArray();

        for (int i = 0; i < answers.size(); i++) {
            JSONObject answerObject = new JSONObject();
            answerObject.put(Constants.QuizAnAnswerKey, answers.get(i));
            answerObject.put(Constants.QuizIsCorrectKey, isCorrect.get(i));
            answersArray.put(answerObject);
        }

        JSONObject o = new JSONObject();
        o.put(Constants.QuizQuestionKey, question);
        o.put(Constants.QuizAnswersKey, answersArray);

        addActivityResultStringsToJSONObject(o);
        return o.toString();
    }
}
