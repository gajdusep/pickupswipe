package com.example.pickupswipe.results;

import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;
import com.example.pickupswipe.enums.SwipeResultActivityTypeSimple;

import org.json.JSONException;
import org.json.JSONObject;

public class NothingResult extends ResultBase {

    public NothingResult() {
        super();
        activityResultDetailed = SwipeResultActivityTypeDetailed.Nothing.toString();
        activityResultSimple = SwipeResultActivityTypeSimple.Nothing.toString();
    }

    @Override
    public String toJSONString() throws JSONException {
        JSONObject o = new JSONObject();
        addActivityResultStringsToJSONObject(o);
        return o.toString();
    }
}
