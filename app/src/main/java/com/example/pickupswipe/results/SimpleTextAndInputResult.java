package com.example.pickupswipe.results;

import com.example.pickupswipe.Constants;
import com.example.pickupswipe.enums.InteractionType;
import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SimpleTextAndInputResult extends CustomResult {
    public String simpleText;

    public SimpleTextAndInputResult(String simpleText) {
        super();
        this.simpleText = simpleText;
        activityResultDetailed = SwipeResultActivityTypeDetailed.SimpleTextAndInput.toString();
    }

    @Override
    public String toJSONString() throws JSONException {
        JSONArray actionsArray = new JSONArray();
        JSONObject simpleTextJSONObject = new JSONObject();
        simpleTextJSONObject.put(Constants.InteractionTypeKey, InteractionType.TextViewType);
        simpleTextJSONObject.put(Constants.ActionTextKey, simpleText);
        actionsArray.put(simpleTextJSONObject);

        JSONObject inputTextObject = new JSONObject();
        inputTextObject.put(Constants.InteractionTypeKey, InteractionType.InputTextType);
        actionsArray.put(inputTextObject);

        JSONObject o = new JSONObject();
        addActionsArrayIntoObject(o, actionsArray);
        addActivityResultStringsToJSONObject(o);
        return o.toString();
    }
}
