package com.example.pickupswipe.results;

import com.example.pickupswipe.Constants;
import com.example.pickupswipe.enums.SwipeResultActivityTypeSimple;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class CustomResult extends ResultBase {
    public CustomResult() {
        super();
        activityResultSimple = SwipeResultActivityTypeSimple.Custom.toString();
    }

    public void addActionsArrayIntoObject(JSONObject o, JSONArray actionsArray) throws JSONException {
        o.put(Constants.SwipeResultActionsArrayKey, actionsArray);
    }
}