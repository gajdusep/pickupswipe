package com.example.pickupswipe.results;

import com.example.pickupswipe.Constants;
import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ResultFactory {

    private String getSimpleTextFromJSONObject(JSONObject o) throws JSONException {
        JSONArray arr = o.getJSONArray(Constants.SwipeResultActionsArrayKey);
        JSONObject simpleTextObject = arr.getJSONObject(0);
        return simpleTextObject.getString(Constants.ActionTextKey);
    }

    public SimpleTextAndImageAndInputResult getImageResultFromJSONObject(JSONObject o) throws JSONException {
        JSONArray arr = o.getJSONArray(Constants.SwipeResultActionsArrayKey);

        JSONObject photoPathObject = arr.getJSONObject(0);
        String photoPath = photoPathObject.getString(Constants.PhotoPathKey);

        JSONObject simpleTextObject = arr.getJSONObject(1);
        String simpleText = simpleTextObject.getString(Constants.ActionTextKey);


        return new SimpleTextAndImageAndInputResult(simpleText, photoPath);
    }

    public QuizResult getQuizResultFromJSONObject(JSONObject o) throws JSONException {
        String question = o.getString(Constants.QuizQuestionKey);

        ArrayList<String> answers = new ArrayList<>();
        ArrayList<Boolean> isCorrect = new ArrayList<>();
        JSONArray answersArray = o.getJSONArray(Constants.QuizAnswersKey);
        for (int i = 0; i < answersArray.length(); i++) {
            JSONObject answerObject = answersArray.getJSONObject(i);
            answers.add(answerObject.getString(Constants.QuizAnAnswerKey));
            isCorrect.add(answerObject.getBoolean(Constants.QuizIsCorrectKey));
        }

        return new QuizResult(question, answers, isCorrect);
    }

    public ResultBase createResultFromJSONString(String jsonString) throws JSONException {
        JSONObject o = new JSONObject(jsonString);
        String activityResultDetailedString = o.getString(Constants.SwipeResultActivityTypeDetailedStringKey);
        SwipeResultActivityTypeDetailed resultActivityTypeDetailed = SwipeResultActivityTypeDetailed.fromString(activityResultDetailedString);
        switch (resultActivityTypeDetailed) {
            case Nothing:
                return new NothingResult();
            case SimpleText:
                return new SimpleTextResult(getSimpleTextFromJSONObject(o));
            case SimpleTextAndInput:
                return new SimpleTextAndInputResult(getSimpleTextFromJSONObject(o));
            case SimpleTextAndPhone:
                return new SimpleTextAndPhoneResult(getSimpleTextFromJSONObject(o));
            case SimpleTextAndImageAndInput:
                return getImageResultFromJSONObject(o);
            case Quiz:
                return getQuizResultFromJSONObject(o);
        }
        return new NothingResult();
    }
}
