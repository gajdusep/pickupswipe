package com.example.pickupswipe;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.pickupswipe.entities.AppRoomDatabase;
import com.example.pickupswipe.entities.PersonEntity;
import com.example.pickupswipe.enums.ResultType;
import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;
import com.example.pickupswipe.results.NothingResult;
import com.example.pickupswipe.results.QuizResult;
import com.example.pickupswipe.results.ResultBase;
import com.example.pickupswipe.results.ResultFactory;
import com.example.pickupswipe.results.SimpleTextAndImageAndInputResult;
import com.example.pickupswipe.results.SimpleTextAndInputResult;
import com.example.pickupswipe.results.SimpleTextAndPhoneResult;
import com.example.pickupswipe.results.SimpleTextResult;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class SwipeResultWrapper {
    public Spinner swipeResultSpinner;
    public TextView chooseAfterSwipeTV;
    public LinearLayout[] linearLayouts;
    public CheckBox[] resultCheckBoxes;
    public EditText[] resultAnswers;
    public EditText resultSimpleEditText;
    public LinearLayout resultSimpleTextLL;
    public LinearLayout resultQuizLL;
    public LinearLayout resultImageLL;
    public Button choosePhotoButton;
    public ImageView chosenPhotoImageView;
    public String chosenImagePath;
    public TextView simpleTextPhoto;
    public EditText resultQuestionET;
    public TextView swipeResultDescription;
    public ResultType resultType;
    private Context context;
    private EditProfileActivity editProfileActivity;
    private SwipeResultActivityTypeDetailed[] swipeResultValues = SwipeResultActivityTypeDetailed.values();

    public SwipeResultWrapper(LinearLayout swipeResultWrapperLL, ResultType resultType, Context context,
                              EditProfileActivity editProfileActivity) {
        this.editProfileActivity = editProfileActivity;
        this.context = context;
        this.swipeResultSpinner = swipeResultWrapperLL.findViewById(R.id.spinnerSwipe);
        this.resultSimpleTextLL = swipeResultWrapperLL.findViewById(R.id.resultSimpleTextLL);
        this.resultQuizLL = swipeResultWrapperLL.findViewById(R.id.resultQuizLL);
        this.resultImageLL = swipeResultWrapperLL.findViewById(R.id.resultImageLL);
        this.linearLayouts = new LinearLayout[]{
                this.resultSimpleTextLL, this.resultQuizLL, this.resultImageLL
        };
        this.resultQuestionET = swipeResultWrapperLL.findViewById(R.id.resultQuestionET);
        this.resultAnswers = new EditText[]{
                resultQuizLL.findViewById(R.id.answer1ET),
                resultQuizLL.findViewById(R.id.answer2ET),
                resultQuizLL.findViewById(R.id.answer3ET),
                resultQuizLL.findViewById(R.id.answer4ET),
        };
        this.resultCheckBoxes = new CheckBox[]{
                resultQuizLL.findViewById(R.id.checkBox1),
                resultQuizLL.findViewById(R.id.checkBox2),
                resultQuizLL.findViewById(R.id.checkBox3),
                resultQuizLL.findViewById(R.id.checkBox4),
        };
        this.choosePhotoButton = swipeResultWrapperLL.findViewById(R.id.choosePhotoButton);
        this.chosenPhotoImageView = swipeResultWrapperLL.findViewById(R.id.chosenPhotoImageView);
        this.simpleTextPhoto = swipeResultWrapperLL.findViewById(R.id.simpleTextPhoto);

        this.resultSimpleEditText = swipeResultWrapperLL.findViewById(R.id.resultSimpleTextIT);
        this.swipeResultDescription = swipeResultWrapperLL.findViewById(R.id.swipeResultDescription);
        this.resultType = resultType;
        this.chooseAfterSwipeTV = swipeResultWrapperLL.findViewById(R.id.chooseAfterSwipeTV);

        prepareEverything();
    }

    private void prepareEverything() {
        this.swipeResultSpinner.setOnItemSelectedListener(new SpinnerOnItemSelectedListener());
        for (LinearLayout l : linearLayouts) {
            l.setVisibility(View.GONE);
        }
        ArrayAdapter adapter = new ArrayAdapter<SwipeResultActivityTypeDetailed>(context,
                R.layout.simple_spinner_item, swipeResultValues);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        swipeResultSpinner.setAdapter(adapter);
        chooseAfterSwipeTV.setText(context.getString(R.string.choose_after_swipe_en, resultType.toString()));

        choosePhotoButton.setOnClickListener(v -> {
            onAddPhotoClick();
        });
    }

    private void onAddPhotoClick() {
        editProfileActivity.setResultImageSwipeResultWrapper(this);
        choosePhotoButton.setText(R.string.change_photo_button_text);
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        editProfileActivity.startActivityForResult(photoPickerIntent, EditProfileActivity.RESULT_IMAGE_LOAD);
    }

    public void changePhoto(Bitmap bitmap) {
        choosePhotoButton.setText(R.string.change_photo_button_text);
        chosenPhotoImageView.setImageBitmap(bitmap);

        final ExecutorService savingService = Executors.newSingleThreadExecutor();
        savingService.submit(() -> {
            deleteCurrentlyLoadedImage();
            String newImageFileName = ImageHandler.saveImage(context, bitmap);
            chosenImagePath = newImageFileName;
        });
    }

    private void deleteCurrentlyLoadedImage() {
        if (chosenImagePath != null && !chosenImagePath.equals("")) {
            ImageHandler.deleteImage(chosenImagePath);
        }
    }

    public void prepareFromJSON(String JSONString) throws JSONException {
        if (JSONString == null || JSONString.equals("")) {
            return;
        }

        ResultFactory factory = new ResultFactory();
        ResultBase result = factory.createResultFromJSONString(JSONString);
        SwipeResultActivityTypeDetailed resultActivity = SwipeResultActivityTypeDetailed.fromString(result.activityResultDetailed);
        int position = Arrays.asList(swipeResultValues).indexOf(resultActivity);
        swipeResultSpinner.setSelection(position);

        switch (resultActivity) {
            case Nothing:
                break;
            case SimpleText:
                resultSimpleEditText.setText(((SimpleTextResult) result).simpleText);
                break;
            case SimpleTextAndInput:
                resultSimpleEditText.setText(((SimpleTextAndInputResult) result).simpleText);
                break;
            case SimpleTextAndPhone:
                resultSimpleEditText.setText(((SimpleTextAndPhoneResult) result).simpleText);
                break;
            case SimpleTextAndImageAndInput:
                SimpleTextAndImageAndInputResult imageAndInputResult = (SimpleTextAndImageAndInputResult) result;
                chosenImagePath = imageAndInputResult.imagePath;
                simpleTextPhoto.setText(imageAndInputResult.simpleText);
                choosePhotoButton.setText(R.string.change_photo_button_text);
                ImageHandler.loadImageToImageView(context, imageAndInputResult.imagePath, chosenPhotoImageView);
                break;
            case Quiz:
                QuizResult quizResult = (QuizResult) result;
                resultQuestionET.setText(quizResult.question);
                for (int i = 0; i < quizResult.answers.size(); i++) {
                    resultAnswers[i].setText(quizResult.answers.get(i));
                    resultCheckBoxes[i].setChecked(quizResult.isCorrect.get(i));
                }
                break;
        }
    }

    public String checkSettings() {
        SwipeResultActivityTypeDetailed chosenResultValue = swipeResultValues[swipeResultSpinner.getSelectedItemPosition()];

        switch (chosenResultValue) {
            case SimpleTextAndImageAndInput:
                if (chosenImagePath == null || chosenImagePath.equals("")) {
                    return context.getString(R.string.result_photo_cannot_be_empty_en);
                }
                break;
            case Quiz:
                int answersFilled = 0;
                for (EditText resultAnswer : resultAnswers) {
                    if (!resultAnswer.getText().toString().equals("")) {
                        answersFilled++;
                    }
                }
                if (answersFilled == 0) {
                    return context.getString(R.string.quiz_must_have_at_least_one_answers);
                }
                break;
        }
        return "";
    }

    public String getSwipeResultsJSON() throws JSONException {
        SwipeResultActivityTypeDetailed chosenResultValue = swipeResultValues[swipeResultSpinner.getSelectedItemPosition()];
        ResultBase result;
        if (chosenResultValue != SwipeResultActivityTypeDetailed.SimpleTextAndImageAndInput) {
            deleteCurrentlyLoadedImage();
        }
        switch (chosenResultValue) {
            case Nothing:
                result = new NothingResult();
                break;
            case SimpleText:
                result = new SimpleTextResult(resultSimpleEditText.getText().toString());
                break;
            case SimpleTextAndInput:
                result = new SimpleTextAndInputResult(resultSimpleEditText.getText().toString());
                break;
            case SimpleTextAndPhone:
                result = new SimpleTextAndPhoneResult(resultSimpleEditText.getText().toString());
                break;
            case SimpleTextAndImageAndInput:
                result = new SimpleTextAndImageAndInputResult(simpleTextPhoto.getText().toString(), chosenImagePath);
                break;
            case Quiz:
                String question = resultQuestionET.getText().toString();
                ArrayList<String> answers = new ArrayList<>();
                ArrayList<Boolean> isCorrect = new ArrayList<>();
                for (int i = 0; i < resultAnswers.length; i++) {
                    if (!resultAnswers[i].getText().toString().equals("")) {
                        answers.add(resultAnswers[i].getText().toString());
                        isCorrect.add(resultCheckBoxes[i].isChecked());
                    }
                }
                result = new QuizResult(question, answers, isCorrect);
                break;
            default:
                result = new NothingResult();
                break;
        }

        return result.toJSONString();
    }

    class SpinnerOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        private String swipeType;

        public SpinnerOnItemSelectedListener() {
            this.swipeType = resultType.toString();
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            for (LinearLayout l : linearLayouts) {
                l.setVisibility(View.GONE);
            }

            switch (swipeResultValues[position]) {
                case Nothing:
                    swipeResultDescription.setText(context.getString(R.string.swipe_result_nothing_description, swipeType));
                    break;
                case SimpleText:
                    resultSimpleTextLL.setVisibility(View.VISIBLE);
                    swipeResultDescription.setText(context.getString(R.string.swipe_result_simple_text_description, swipeType));
                    break;
                case SimpleTextAndInput:
                    resultSimpleTextLL.setVisibility(View.VISIBLE);
                    swipeResultDescription.setText(context.getString(R.string.swipe_result_simple_text_and_input_description, swipeType));
                    break;
                case SimpleTextAndPhone:
                    resultSimpleTextLL.setVisibility(View.VISIBLE);
                    swipeResultDescription.setText(context.getString(R.string.swipe_result_simple_text_and_phone_description, swipeType));
                    break;
                case SimpleTextAndImageAndInput:
                    resultImageLL.setVisibility(View.VISIBLE);
                    swipeResultDescription.setText(context.getString(R.string.swipe_result_simple_text_and_image_and_input_description, swipeType));
                    break;
                case Quiz:
                    resultQuizLL.setVisibility(View.VISIBLE);
                    swipeResultDescription.setText(context.getString(R.string.swipe_result_quiz_description, swipeType));
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

}

public class EditProfileActivity extends AppCompatActivity {
    public static final int PROFILE_IMAGE_LOAD = 111;
    public static final int RESULT_IMAGE_LOAD = 112;
    AppRoomDatabase appRoomDatabase;
    SwipeResultWrapper wrapper;
    private int personID;
    private PersonEntity personEntity;
    private boolean update;
    private EditText personNameEditText;
    private EditText descriptionEditText;
    private SeekBar ageSeekBar;
    private TextView currentAgeTextView;
    private ArrayList<Integer> photoFramesIDs;
    private String[] photoPathsArray;
    private SwipeResultWrapper likeSwipeResultWrapper;
    private SwipeResultWrapper dislikeSwipeResultWrapper;
    private SwipeResultWrapper superlikeSwipeResultWrapper;
    private Context context;
    private int currentImageIndex;
    private int minAge = 15;
    private int defaultAge = 22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setTitle(R.string.create_new_profile_title_en);
        context = this;
        appRoomDatabase = ((App) getApplication()).appRoomDatabase;

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        photoPathsArray = new String[]{
                null, null, null,
                null, null, null
        };

        findViews();
        prepareAgeProgressBar();

        personID = getIntent().getExtras().getInt(SwipeSettingsActivity.PERSON_ID_KEY, -1);

        if (personID != -1) {
            update = true;
            Executor myExecutor = Executors.newSingleThreadExecutor();
            myExecutor.execute(() -> {
                personEntity = appRoomDatabase.personDao().getPersonEntityById(personID);
                populateViews(personEntity);
            });
        } else {
            update = false;
            personEntity = new PersonEntity();
            preparePhotoFrames();
        }
    }

    private AlertDialog.Builder getOKAlertDialogBuilder(String info) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setMessage(info).setPositiveButton("OK", ((dialog, which) -> {
        }));
        return adb;
    }

    private void findViews() {
        personNameEditText = findViewById(R.id.personNameEditText);
        descriptionEditText = findViewById(R.id.descriptionEditText);

        ageSeekBar = findViewById(R.id.ageSeekBar);
        currentAgeTextView = findViewById(R.id.currentAgeTextView);

        LinearLayout likeSwipeResultWrapperLL = findViewById(R.id.like_swipe_result_wrapper_ll);
        LinearLayout dislikeSwipeResultWrapperLL = findViewById(R.id.dislike_swipe_result_wrapper_ll);
        LinearLayout superlikeSwipeResultWrapperLL = findViewById(R.id.superlike_swipe_result_wrapper_ll);
        likeSwipeResultWrapper = new SwipeResultWrapper(likeSwipeResultWrapperLL, ResultType.Like, this, this);
        dislikeSwipeResultWrapper = new SwipeResultWrapper(dislikeSwipeResultWrapperLL, ResultType.Dislike, this, this);
        superlikeSwipeResultWrapper = new SwipeResultWrapper(superlikeSwipeResultWrapperLL, ResultType.Superlike, this, this);

        photoFramesIDs = new ArrayList<>(Arrays.asList(
                R.id.photo_edit_1, R.id.photo_edit_2, R.id.photo_edit_3,
                R.id.photo_edit_4, R.id.photo_edit_5, R.id.photo_edit_6)
        );
    }

    private void prepareAgeProgressBar() {
        setAgeProgress(defaultAge);
        currentAgeTextView.setText(String.valueOf(defaultAge));
        ageSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentAgeTextView.setText(String.valueOf(progress + minAge));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void setAgeProgress(int age) {
        ageSeekBar.setProgress(age - minAge);
    }

    private int getAgeFromSeekbar() {
        return ageSeekBar.getProgress() + minAge;
    }

    private void populateViews(PersonEntity personEntity) {
        if (personEntity.name != null) {
            personNameEditText.setText(personEntity.name);
        }

        preparePhotoFrames();

        if (personEntity.description != null) {
            descriptionEditText.setText(personEntity.description);
        }
        setAgeProgress(personEntity.age);

        try {
            likeSwipeResultWrapper.prepareFromJSON(personEntity.swipeRightResultJSON);
            dislikeSwipeResultWrapper.prepareFromJSON(personEntity.swipeLeftResultJSON);
            superlikeSwipeResultWrapper.prepareFromJSON(personEntity.swipeSuperResultJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void preparePhotoFrames() {
        for (int i = 0; i < photoFramesIDs.size(); i++) {
            int photoFrameID = photoFramesIDs.get(i);
            FrameLayout frameLayout = findViewById(photoFrameID);
            ImageView iv = frameLayout.findViewById(R.id.imagePhotoIV);
            ImageButton ib = frameLayout.findViewById(R.id.changeImageIB);
            ProgressBar pb = frameLayout.findViewById(R.id.photoProgressBar);

            final int index = i;
            if (personEntity.imagesPaths.size() > index) {
                String path = personEntity.imagesPaths.get(index);
                photoPathsArray[index] = path;

                setProgressBarVisible(index);

                Glide.with(context).asBitmap().load(path)
                        .apply(new RequestOptions().override(200, 200))
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource,
                                                        @Nullable Transition<? super Bitmap> transition) {
                                setImageButtonToDelete(index);
                                setProgressBarGone(index);
                                iv.setImageBitmap(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
            } else {
                ib.setOnClickListener(v -> onAddPhotoClick(index));
            }
        }
    }

    private void onAddPhotoClick(int index) {
        setImageButtonToDelete(index);
        setProgressBarVisible(index);

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        currentImageIndex = index;
        startActivityForResult(photoPickerIntent, PROFILE_IMAGE_LOAD);
    }

    private void onDeletePhotoClick(int index) {
        setImageButtonToAdd(index);
        ImageHandler.deleteImage(photoPathsArray[index]);
        photoPathsArray[index] = null;

        FrameLayout frameLayout = findViewById(photoFramesIDs.get(index));
        ImageView iv = frameLayout.findViewById(R.id.imagePhotoIV);
        iv.setImageBitmap(null);
    }

    private void setImageButtonToDelete(int index) {
        FrameLayout frameLayout = findViewById(photoFramesIDs.get(index));
        ImageButton imageButton = frameLayout.findViewById(R.id.changeImageIB);
        imageButton.setImageResource(R.drawable.ic_baseline_delete_24);
        imageButton.setOnClickListener(v -> {
            onDeletePhotoClick(index);
        });
    }

    private void setImageButtonToAdd(int index) {
        FrameLayout frameLayout = findViewById(photoFramesIDs.get(index));
        ImageButton imageButton = frameLayout.findViewById(R.id.changeImageIB);
        imageButton.setImageResource(R.drawable.ic_baseline_add_circle_24);
        imageButton.setOnClickListener(v -> {
            onAddPhotoClick(index);
        });
    }

    private void setProgressBarVisible(int index) {
        FrameLayout frameLayout = findViewById(photoFramesIDs.get(index));
        ProgressBar pb = frameLayout.findViewById(R.id.photoProgressBar);
        pb.setVisibility(View.VISIBLE);
    }

    private void setProgressBarGone(int index) {
        FrameLayout frameLayout = findViewById(photoFramesIDs.get(index));
        ProgressBar pb = frameLayout.findViewById(R.id.photoProgressBar);
        pb.setVisibility(View.INVISIBLE);
    }

    private void fillImageFrameAtIndex(int index, Bitmap bitmap) {
        String newImageFileName = ImageHandler.saveImage(context, bitmap);
        photoPathsArray[index] = newImageFileName;

        FrameLayout frameLayout = findViewById(photoFramesIDs.get(index));
        ImageView iv = frameLayout.findViewById(R.id.imagePhotoIV);

        iv.setImageBitmap(bitmap);
        setImageButtonToDelete(index);
        setProgressBarGone(index);
    }

    public void setResultImageSwipeResultWrapper(SwipeResultWrapper wrapper) {
        this.wrapper = wrapper;
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (reqCode == PROFILE_IMAGE_LOAD) {
            if (resultCode == RESULT_OK) {
                final Uri imageUri = data.getData();

                Glide.with(this)
                        .asBitmap().load(imageUri)
                        .apply(new RequestOptions().override(800, 1000))
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource,
                                                        @Nullable Transition<? super Bitmap> transition) {
                                fillImageFrameAtIndex(currentImageIndex, resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
            } else {
                setProgressBarGone(currentImageIndex);
            }
        }
        if (reqCode == RESULT_IMAGE_LOAD) {
            if (resultCode == RESULT_OK) {
                final Uri imageUri = data.getData();

                Glide.with(this)
                        .asBitmap()
                        .load(imageUri).apply(new RequestOptions().override(500, 500))
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource,
                                                        @Nullable Transition<? super Bitmap> transition) {
                                wrapper.changePhoto(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_new_profile_menu, menu);
        return true;
    }

    private void saveNewProfile() {
        personEntity.name = personNameEditText.getText().toString();
        personEntity.description = descriptionEditText.getText().toString();
        personEntity.age = getAgeFromSeekbar();

        String likeSwipeResultJSON = "", dislikeSwipeResultJSON = "", superlikeSwipeResultJSON = "";
        try {
            likeSwipeResultJSON = likeSwipeResultWrapper.getSwipeResultsJSON();
            dislikeSwipeResultJSON = dislikeSwipeResultWrapper.getSwipeResultsJSON();
            superlikeSwipeResultJSON = superlikeSwipeResultWrapper.getSwipeResultsJSON();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        personEntity.swipeRightResultJSON = likeSwipeResultJSON;
        personEntity.swipeLeftResultJSON = dislikeSwipeResultJSON;
        personEntity.swipeSuperResultJSON = superlikeSwipeResultJSON;

        personEntity.imagesPaths = new ArrayList<>();
        for (String imagePath : photoPathsArray) {
            if (imagePath != null) {
                personEntity.imagesPaths.add(imagePath);
            }
        }

        Executor myExecutor = Executors.newSingleThreadExecutor();
        if (!update) {
            myExecutor.execute(() -> {
                appRoomDatabase.personDao().insertAll(personEntity);
            });
        } else {
            myExecutor.execute(() -> {
                appRoomDatabase.personDao().update(personEntity);
            });
        }
    }

    public boolean checkFields() {
        if (personNameEditText.getText().toString().equals("")) {
            getOKAlertDialogBuilder(getString(R.string.name_cannot_be_empty_en)).create().show();
            return false;
        }

        int countNotNull = 0;
        for (String imagePath : photoPathsArray) {
            if (imagePath != null) {
                countNotNull++;
                break;
            }
        }
        if (countNotNull == 0) {
            getOKAlertDialogBuilder(getString(R.string.photos_cannot_be_empty_en)).create().show();
            return false;
        }

        SwipeResultWrapper[] wrappers = new SwipeResultWrapper[]{
                likeSwipeResultWrapper, dislikeSwipeResultWrapper, superlikeSwipeResultWrapper
        };
        for (SwipeResultWrapper wrapper : wrappers) {
            String checkResult = wrapper.checkSettings();
            if (!checkResult.equals("")) {
                getOKAlertDialogBuilder(checkResult).create().show();
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save_new_profile) {
            if (checkFields()) {
                saveNewProfile();
                finish();
            }
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle("Exit?");
        adb.setMessage("Do you want to exit without saving?");
        adb.setNegativeButton("Cancel", null);
        adb.setPositiveButton("Yes", (dialog, which) -> finish());
        adb.show();
    }
}
