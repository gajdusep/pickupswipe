package com.example.pickupswipe;

import android.app.Application;

import androidx.room.Room;

import com.example.pickupswipe.entities.AppRoomDatabase;


public class App extends Application {
    public AppRoomDatabase appRoomDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        appRoomDatabase = Room.databaseBuilder(getApplicationContext(),
                AppRoomDatabase.class, "database-name").build();
    }
}
