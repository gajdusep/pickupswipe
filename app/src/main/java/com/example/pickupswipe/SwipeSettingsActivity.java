package com.example.pickupswipe;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pickupswipe.entities.AppRoomDatabase;
import com.example.pickupswipe.entities.PersonEntity;
import com.example.pickupswipe.entities.PersonViewModel;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

interface PersonClickListener {
    void onDeleteButtonClicked(PersonEntity personEntity);

    void onEditButtonClicked(PersonEntity personEntity);
}

class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonViewHolder> {

    private List<PersonEntity> data;
    private LayoutInflater layoutInflater;
    private PersonClickListener personClickListener;
    private Context context;

    public PersonAdapter(Context context, PersonClickListener listener) {
        this.data = new ArrayList<>();
        this.personClickListener = listener;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.profile_list_item, parent, false);
        return new PersonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<PersonEntity> newData) {
        if (data != null) {
            data.clear();
            data.addAll(newData);
            notifyDataSetChanged();
        } else {
            // first initialization
            data = newData;
        }
    }

    class PersonViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private CircleImageView profileImageView;
        private ImageButton deleteProfileButton;
        private ImageButton editProfileButton;

        PersonViewHolder(View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.nameTextView);
            profileImageView = itemView.findViewById(R.id.profileImageView);
            deleteProfileButton = itemView.findViewById(R.id.deleteProfileButton);
            editProfileButton = itemView.findViewById(R.id.editProfileButton);
        }

        void bind(final PersonEntity personEntity) {
            if (personEntity != null) {
                ImageHandler.loadImageToImageView(context, personEntity.imagesPaths.get(0), profileImageView, 200, 200);

                String nameText = personEntity.name + ", " + personEntity.age;
                nameTextView.setText(nameText);
                deleteProfileButton.setOnClickListener(v -> {
                    if (personClickListener != null)
                        personClickListener.onDeleteButtonClicked(personEntity);
                });
                editProfileButton.setOnClickListener(v -> {
                    if (personClickListener != null) {
                        personClickListener.onEditButtonClicked(personEntity);
                    }
                });

            }
        }
    }
}

public class SwipeSettingsActivity extends AppCompatActivity implements PersonClickListener {
    public static int ADD_PROFILE = 0;
    public static String PERSON_ID_KEY = "NEW_PROFILE_KEY";
    private RecyclerView recyclerView;
    private PersonAdapter personAdapter;
    private PersonViewModel personViewModel;
    private Context context;
    private AppRoomDatabase appRoomDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_settings);
        setTitle(R.string.settings_title_en);
        context = this;
        recyclerView = findViewById(R.id.personsRV);
        appRoomDatabase = ((App) getApplication()).appRoomDatabase;

        personAdapter = new PersonAdapter(this, this);
        setAdapter();

        personViewModel = new ViewModelProvider(this).get(PersonViewModel.class);
        personViewModel.getAllPersons().observe(this, posts -> personAdapter.setData(posts));
    }

    void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(personAdapter);
    }

    @Override
    public void onDeleteButtonClicked(PersonEntity personEntity) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle("Delete?");
        adb.setMessage("Are you sure you want to delete " + personEntity.name);
        adb.setNegativeButton("Cancel", null);
        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                personViewModel.deletePerson(personEntity);
            }
        });
        adb.show();
    }

    @Override
    public void onEditButtonClicked(PersonEntity personEntity) {
        Intent editProfileIntent = new Intent(this, EditProfileActivity.class);
        editProfileIntent.putExtra(PERSON_ID_KEY, personEntity.uid);
        startActivity(editProfileIntent);
    }

    public void addProfileButtonClicked(View v) {
        Intent editProfileIntent = new Intent(this, EditProfileActivity.class);
        editProfileIntent.putExtra(PERSON_ID_KEY, -1);
        startActivity(editProfileIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
