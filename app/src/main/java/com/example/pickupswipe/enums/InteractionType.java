package com.example.pickupswipe.enums;

public enum InteractionType {
    TextViewType("textview"),
    InputTextType("inputtext"),
    InputPhoneType("inputphone"),
    PhotoType("photo");

    private String enumString;

    InteractionType(String enumString) {
        this.enumString = enumString;
    }

    public static InteractionType fromString(String text) throws IllegalArgumentException {
        for (InteractionType r : InteractionType.values()) {
            if (r.toString().equals(text)) {
                return r;
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found");
    }

    @Override
    public String toString() {
        return enumString;
    }
}
