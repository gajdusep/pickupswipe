package com.example.pickupswipe.enums;

public enum SwipeResultActivityTypeDetailed {
    Nothing("Nothing"), // default, don't do anything (even default in swipe right)
    SimpleText("Simple text"), // simple text without anything else
    SimpleTextAndInput("Text and input"), // simple text, after that, text input
    SimpleTextAndPhone("Text and phone number"), // simple text, after that, phone input
    SimpleTextAndImageAndInput("Text, image and input"), // text, image and text input
    Quiz("Question with answers"); // a quiz with answers

    private String enumString;

    SwipeResultActivityTypeDetailed(String enumString) {
        this.enumString = enumString;
    }

    public static SwipeResultActivityTypeDetailed fromString(String text) throws IllegalArgumentException {
        for (SwipeResultActivityTypeDetailed b : SwipeResultActivityTypeDetailed.values()) {
            if (b.toString().equals(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found");
    }

    @Override
    public String toString() {
        return enumString;
    }
}

