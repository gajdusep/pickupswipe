package com.example.pickupswipe.enums;

public enum ResultType {
    Like("like"),
    Dislike("dislike"),
    Superlike("superlike");

    private String enumString;

    ResultType(String enumString) {
        this.enumString = enumString;
    }

    public static ResultType fromString(String text) throws IllegalArgumentException {
        for (ResultType r : ResultType.values()) {
            if (r.toString().equals(text)) {
                return r;
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found");
    }

    @Override
    public String toString() {
        return enumString;
    }
}
