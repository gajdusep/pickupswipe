package com.example.pickupswipe.enums;

public enum SwipeResultActivityTypeSimple {
    Nothing("Nothing"), // default, don't do anything
    Custom("Custom actions"),
    Quiz("Quiz");

    private String enumString;

    SwipeResultActivityTypeSimple(String enumString) {
        this.enumString = enumString;
    }

    public static SwipeResultActivityTypeSimple fromString(String text) throws IllegalArgumentException {
        for (SwipeResultActivityTypeSimple b : SwipeResultActivityTypeSimple.values()) {
            if (b.toString().equals(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found");
    }

    @Override
    public String toString() {
        return enumString;
    }
}
