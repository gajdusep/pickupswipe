package com.example.pickupswipe;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    FrameLayout informationPanel;
    ImageButton closeInfoPanelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        informationPanel = findViewById(R.id.information_panel);
        informationPanel.setVisibility(View.INVISIBLE);
        closeInfoPanelButton = informationPanel.findViewById(R.id.close_info_panel_button);
        closeInfoPanelButton.setOnClickListener(v -> hideInfoPanel());
    }

    @Override
    protected void onStart() {
        super.onStart();
        ImageHandler.logAllImages(this);
    }

    public void goToPrepare(View v) {
        Intent swipeSettingsIntent = new Intent(this, SwipeSettingsActivity.class);
        startActivity(swipeSettingsIntent);
    }

    public void goSwiping(View v) {
        Intent swipingIntent = new Intent(this, SwipeActivity.class);
        startActivity(swipingIntent);
    }

    public void goToMatches(View v) {
        Intent matchesIntent = new Intent(this, MatchesActivity.class);
        startActivity(matchesIntent);
    }

    public void showInfoPanel(View v) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.bottom_up_anim);
        informationPanel.startAnimation(bottomUp);
        informationPanel.setVisibility(View.VISIBLE);
    }

    public void hideInfoPanel() {
        Animation bottomDown = AnimationUtils.loadAnimation(this, R.anim.bottom_down_anim);
        informationPanel.startAnimation(bottomDown);
        informationPanel.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (informationPanel.getVisibility() == View.VISIBLE) {
            hideInfoPanel();
        } else {
            super.onBackPressed();
        }
    }
}