package com.example.pickupswipe.entities;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {
        MessageEntity.class,
        PersonEntity.class
}, version = 1)
public abstract class AppRoomDatabase extends RoomDatabase {
    public abstract MessageDao messageDao();

    public abstract PersonDao personDao();
}