package com.example.pickupswipe.entities;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MessageDao {
    @Query("SELECT * FROM messageEntity ORDER BY datetime DESC")
    LiveData<List<MessageEntity>> getAll();

    @Query("SELECT * FROM messageEntity WHERE uid IN (:userIds)")
    List<MessageEntity> loadAllByIds(int[] userIds);

    @Insert
    void insertAll(MessageEntity... messages);

    @Delete
    void delete(MessageEntity message);
}