package com.example.pickupswipe.entities;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.pickupswipe.App;
import com.example.pickupswipe.ImageHandler;
import com.example.pickupswipe.enums.SwipeResultActivityTypeDetailed;
import com.example.pickupswipe.results.ResultBase;
import com.example.pickupswipe.results.ResultFactory;
import com.example.pickupswipe.results.SimpleTextAndImageAndInputResult;

import org.json.JSONException;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PersonViewModel extends AndroidViewModel {

    AppRoomDatabase appRoomDatabase;
    private PersonDao personDao;
    private ExecutorService executorService;

    public PersonViewModel(@NonNull Application application) {
        super(application);
        appRoomDatabase = ((App) getApplication()).appRoomDatabase;
        personDao = appRoomDatabase.personDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<PersonEntity>> getAllPersons() {
        return personDao.getAll();
    }

    public void savePerson(PersonEntity person) {
        executorService.execute(() -> personDao.insertAll(person));
    }

    public void deletePerson(PersonEntity person) {
        for (String path : person.imagesPaths) {
            ImageHandler.deleteImage(path);
        }
        String[] JSONStrings = new String[]{
                person.swipeLeftResultJSON, person.swipeRightResultJSON, person.swipeSuperResultJSON
        };
        ResultFactory factory = new ResultFactory();
        for (String s : JSONStrings) {
            try {
                ResultBase result = null;
                result = factory.createResultFromJSONString(s);
                SwipeResultActivityTypeDetailed resultActivity = SwipeResultActivityTypeDetailed.fromString(result.activityResultDetailed);

                if (resultActivity == SwipeResultActivityTypeDetailed.SimpleTextAndImageAndInput) {
                    SimpleTextAndImageAndInputResult imageAndInputResult = (SimpleTextAndImageAndInputResult) result;
                    ImageHandler.deleteImage(imageAndInputResult.imagePath);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        executorService.execute(() -> personDao.delete(person));
    }

    public void updatePerson(PersonEntity person) {
        executorService.execute(() -> personDao.update(person));
    }
}