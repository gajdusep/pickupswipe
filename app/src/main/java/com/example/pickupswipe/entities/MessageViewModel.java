package com.example.pickupswipe.entities;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.pickupswipe.App;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageViewModel extends AndroidViewModel {

    AppRoomDatabase appRoomDatabase;
    private MessageDao messageDao;
    private ExecutorService executorService;

    public MessageViewModel(@NonNull Application application) {
        super(application);
        appRoomDatabase = ((App) getApplication()).appRoomDatabase;
        messageDao = appRoomDatabase.messageDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<MessageEntity>> getAllMessages() {
        return messageDao.getAll();
    }

    public void deleteMessage(MessageEntity messageEntity) {
        executorService.execute(() -> messageDao.delete(messageEntity));
    }
}
