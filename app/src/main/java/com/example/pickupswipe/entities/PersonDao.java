package com.example.pickupswipe.entities;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PersonDao {
    @Query("SELECT * FROM PersonEntity")
    LiveData<List<PersonEntity>> getAll();

    @Query("SELECT * FROM PersonEntity WHERE uid IN (:userIds)")
    LiveData<List<PersonEntity>> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM PersonEntity WHERE uid = :id")
    PersonEntity getPersonEntityById(int id);

    @Insert
    void insertAll(PersonEntity... persons);

    @Update
    void update(PersonEntity person);

    @Delete
    void delete(PersonEntity person);
}