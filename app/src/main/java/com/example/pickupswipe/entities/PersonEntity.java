package com.example.pickupswipe.entities;


import android.graphics.Bitmap;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;


class PersonConverters {
    @TypeConverter
    public static ArrayList<String> fromString(String value) {
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(ArrayList<String> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }
}

@Entity(tableName = "PersonEntity")
@TypeConverters({PersonConverters.class})
public class PersonEntity {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "age")
    public int age;

    @Ignore
    public ArrayList<Bitmap> images;

    @ColumnInfo(name = "images_paths")
    public ArrayList<String> imagesPaths;

    @ColumnInfo(name = "swipe_right_result_json")
    public String swipeRightResultJSON;

    @ColumnInfo(name = "swipe_left_result_json")
    public String swipeLeftResultJSON;

    @ColumnInfo(name = "swipe_super_result_json")
    public String swipeSuperResultJSON;

    public PersonEntity() {
        images = new ArrayList<>();
        imagesPaths = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "PersonEntity{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", age=" + age +
                ", images=" + images +
                ", imagesPaths=" + imagesPaths +
                ", swipeRightResultJSON='" + swipeRightResultJSON + '\'' +
                ", swipeLeftResultJSON='" + swipeLeftResultJSON + '\'' +
                ", swipeSuperResultJSON='" + swipeSuperResultJSON + '\'' +
                '}';
    }
}
