package com.example.pickupswipe.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class MessageEntity {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "message_json")
    public String messageJSON;

    @ColumnInfo(name = "datetime")
    public long datetime;

    @ColumnInfo(name = "profile_id")
    public int profileID;

    @ColumnInfo(name = "result_type_str")
    public String resultTypeString;

    @Override
    public String toString() {
        return "MessageEntity{" +
                "uid=" + uid +
                ", messageJSON='" + messageJSON + '\'' +
                ", datetime=" + datetime +
                ", profileID=" + profileID +
                ", resultTypeString='" + resultTypeString + '\'' +
                '}';
    }
}
