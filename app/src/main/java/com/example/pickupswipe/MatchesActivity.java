package com.example.pickupswipe;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.pickupswipe.entities.AppRoomDatabase;
import com.example.pickupswipe.entities.MessageEntity;
import com.example.pickupswipe.entities.MessageViewModel;
import com.example.pickupswipe.entities.PersonDao;
import com.example.pickupswipe.entities.PersonEntity;
import com.example.pickupswipe.enums.InteractionType;
import com.example.pickupswipe.enums.ResultType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


interface MessageClickListener {
    void onDeleteButtonClicked(MessageEntity messageEntity);
}

class InnerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private int count;
    private List<JSONObject> jsonObjects;

    public InnerAdapter(List<JSONObject> jsonObjects, Context context) {
        this.jsonObjects = jsonObjects;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.simple_message, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder holder = (ViewHolder) viewHolder;
        JSONObject o = jsonObjects.get(i);

        try {
            if (o.getString(Constants.ItemTypeKey).equals(InteractionType.PhotoType.toString())) {
                String text = Constants.YouString + ": ";
                holder.singleMessageTV.setText(text);
                String path = o.getString(Constants.YouString);
                Glide.with(context).asBitmap().load(path)
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                super.onLoadFailed(errorDrawable);
                                String errorText = Constants.YouString + ": " + context.getString(R.string.photo_deleted);
                                holder.singleMessageTV.setText(errorText);
                            }

                            @Override
                            public void onResourceReady(@NonNull Bitmap resource,
                                                        @Nullable Transition<? super Bitmap> transition) {
                                holder.messagePhoto.setImageBitmap(resource);
                                holder.messagePhoto.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
            } else {
                String text = "";
                if (o.has(Constants.YouString)) {
                    text = Constants.YouString + ": " + o.getString(Constants.YouString);
                } else if (o.has(Constants.ThemString)) {
                    text = Constants.ThemString + ": " + o.getString(Constants.ThemString);
                    holder.singleMessageTV.setTypeface(null, Typeface.BOLD);
                }
                holder.singleMessageTV.setText(text);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("json error", e.toString());
        }
    }

    @Override
    public int getItemCount() {
        return jsonObjects.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView singleMessageTV;
        private ImageView messagePhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            singleMessageTV = itemView.findViewById(R.id.simpleMessageTV);
            messagePhoto = itemView.findViewById(R.id.messagePhoto);
            messagePhoto.setVisibility(View.GONE);
        }
    }
}

class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private List<MessageEntity> data;
    private LayoutInflater layoutInflater;
    private MessageClickListener messageClickListener;
    private PersonDao personDao;
    private Context context;

    public MessageAdapter(Context context, MessageClickListener listener, PersonDao personDao) {
        this.data = new ArrayList<>();
        this.context = context;
        this.messageClickListener = listener;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.personDao = personDao;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.message_list_item, parent, false);
        return new MessageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<MessageEntity> newData) {
        if (data != null) {
            data.clear();
            data.addAll(newData);
            notifyDataSetChanged();
        } else {
            // first initialization
            data = newData;
        }
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout messageLL;
        public TextView profileNameTV;
        public ImageButton deleteButton;
        public TextView datetimeTV;
        public ImageView resultIconIV;
        public RecyclerView messagesRV;

        MessageViewHolder(View itemView) {
            super(itemView);
            messageLL = itemView.findViewById(R.id.messageLL);
            profileNameTV = itemView.findViewById(R.id.profileNameTV);
            deleteButton = itemView.findViewById(R.id.deleteMessageButton);
            datetimeTV = itemView.findViewById(R.id.datetimeTV);
            resultIconIV = itemView.findViewById(R.id.resultIconIV);
            messagesRV = itemView.findViewById(R.id.messagesRV);
        }

        void bind(final MessageEntity messageEntity) {
            if (messageEntity != null) {
                setIconAndColor(messageEntity);

                ArrayList<JSONObject> jsonObjects = new ArrayList<>();
                try {
                    jsonObjects = getJSONObjects(messageEntity);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                InnerAdapter innerAdapter = new InnerAdapter(jsonObjects, context);

                LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                messagesRV.setLayoutManager(layoutManager);
                messagesRV.setAdapter(innerAdapter);

                datetimeTV.setText(Constants.datetimeToString(messageEntity.datetime));
                deleteButton.setOnClickListener(v -> {
                    if (messageClickListener != null)
                        messageClickListener.onDeleteButtonClicked(messageEntity);
                });
                ExecutorService executorService = Executors.newSingleThreadExecutor();
                executorService.execute(() -> {
                    PersonEntity personEntity = personDao.getPersonEntityById(messageEntity.profileID);
                    if (personEntity != null) {
                        profileNameTV.setText(personEntity.name);
                    } else {
                        profileNameTV.setText(R.string.profiled_deleted);
                    }
                });
            }
        }

        ArrayList<JSONObject> getJSONObjects(MessageEntity messageEntity) throws JSONException {
            JSONObject messageJSON = new JSONObject(messageEntity.messageJSON);
            JSONArray jsonArray = messageJSON.getJSONArray(Constants.AnswerArrayString);

            ArrayList<JSONObject> jsonObjects = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject o = (JSONObject) jsonArray.get(i);
                jsonObjects.add(o);
            }
            return jsonObjects;
        }

        void setIconAndColor(MessageEntity messageEntity) {
            String r = messageEntity.resultTypeString;

            int color = 0;
            int icon = 0;
            if (r.equals(ResultType.Like.toString())) {
                color = R.color.likeColor;
                icon = R.drawable.ic_baseline_like_24;
            } else if (r.equals(ResultType.Dislike.toString())) {
                color = R.color.dislikeColor;
                icon = R.drawable.ic_baseline_dislike_24;
            } else {
                color = R.color.superlikeColor;
                icon = R.drawable.ic_baseline_superlike_24;
            }

            resultIconIV.setImageResource(icon);
            resultIconIV.setColorFilter(context.getResources().getColor(color));
        }
    }
}

public class MatchesActivity extends AppCompatActivity implements MessageClickListener {
    private Context context;
    private RecyclerView messagesRV;
    private MessageAdapter messageAdapter;
    private MessageViewModel messageViewModel;
    private AppRoomDatabase appRoomDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);
        setTitle(R.string.matches_list_en);
        context = this;
        messagesRV = findViewById(R.id.messagesRV);
        appRoomDatabase = ((App) getApplication()).appRoomDatabase;

        messageAdapter = new MessageAdapter(this, this, appRoomDatabase.personDao());
        messagesRV.setLayoutManager(new LinearLayoutManager(this));
        messagesRV.setHasFixedSize(true);
        messagesRV.setItemAnimator(new DefaultItemAnimator());
        messagesRV.setAdapter(messageAdapter);
        messageViewModel = new ViewModelProvider(this).get(MessageViewModel.class);
        messageViewModel.getAllMessages().observe(this, posts -> messageAdapter.setData(posts));

        messagesRV.smoothScrollToPosition(0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        messagesRV.smoothScrollToPosition(0);
    }

    @Override
    public void onDeleteButtonClicked(MessageEntity messageEntity) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);

        adb.setTitle("Delete?");
        adb.setMessage("Are you sure you want to delete this message from " +
                Constants.datetimeToString(messageEntity.datetime));
        adb.setNegativeButton("Cancel", null);
        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                messageViewModel.deleteMessage(messageEntity);
            }
        });
        adb.show();
    }
}
