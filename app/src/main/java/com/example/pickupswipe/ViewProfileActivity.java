package com.example.pickupswipe;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pickupswipe.entities.PersonDao;
import com.example.pickupswipe.entities.PersonEntity;
import com.example.pickupswipe.enums.ResultType;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ViewProfileActivity extends AppCompatActivity {
    Context context;
    CarouselView carouselView;
    PersonEntity personEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        this.context = this;

        PersonDao personDao = ((App) getApplication()).appRoomDatabase.personDao();
        Intent intent = getIntent();
        int id = intent.getExtras().getInt(SwipeActivity.VIEW_PROFILE_ID_KEY);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            personEntity = personDao.getPersonEntityById(id);
            prepareActivity();
        });
    }

    public void prepareActivity() {
        TextView nameTV = findViewById(R.id.nameTV);
        TextView descriptionTV = findViewById(R.id.descriptionTV);

        String nameAge = personEntity.name + ", " + personEntity.age;
        nameTV.setText(nameAge);
        descriptionTV.setText(personEntity.description);

        ViewListener viewListener = position -> {
            View customView = getLayoutInflater().inflate(R.layout.simple_image, null);
            ImageView imageView = customView.findViewById(R.id.simple_image_view);
            ImageHandler.loadImageToImageView(context, personEntity.imagesPaths.get(position), imageView);
            return customView;
        };
        carouselView = findViewById(R.id.carouselView);
        carouselView.setViewListener(viewListener);
        carouselView.setPageCount(personEntity.imagesPaths.size());
    }

    private void endWithResult(String result) {
        Intent data = new Intent();
        data.putExtra(SwipeActivity.VIEW_PROFILE_RESULT_KEY, result);
        setResult(RESULT_OK, data);
        finish();
    }

    public void onLikeClick(View v) {
        endWithResult(ResultType.Like.toString());
    }

    public void onDislikeClick(View v) {
        endWithResult(ResultType.Dislike.toString());
    }

    public void onSuperlikeClick(View v) {
        endWithResult(ResultType.Superlike.toString());
    }

    public void onHideClick(View v) {
        endWithResult("");
    }
}
