package com.example.pickupswipe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pickupswipe.entities.PersonEntity;
import com.example.pickupswipe.entities.PersonViewModel;
import com.example.pickupswipe.enums.ResultType;
import com.example.pickupswipe.enums.SwipeResultActivityTypeSimple;
import com.example.pickupswipe.swiperesultactivities.CustomResultActivity;
import com.example.pickupswipe.swiperesultactivities.QuizResultActivity;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.Duration;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


class PersonProfileCardAdapter extends RecyclerView.Adapter<PersonProfileCardAdapter.PersonViewHolder> {

    public List<PersonEntity> profiles;
    Context context;
    LayoutInflater layoutInflater;

    public PersonProfileCardAdapter(Context context, List<PersonEntity> profiles) {
        this.context = context;
        this.profiles = profiles;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.card, parent, false);
        return new PersonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        PersonEntity personEntity = profiles.get(position);
        String nameAge = personEntity.name + ", " + personEntity.age;
        holder.cardName.setText(nameAge);

        if (personEntity.imagesPaths != null && personEntity.imagesPaths.size() != 0) {
            ImageHandler.loadImageToImageView(context, personEntity.imagesPaths.get(0), holder.cardImage, 600, 900);
        } else {
            holder.cardImage.setImageResource(R.drawable.ic_launcher_background);
        }

        holder.cardImage.setOnClickListener(v -> {
            Intent viewProfileIntent = new Intent(context, ViewProfileActivity.class);
            viewProfileIntent.putExtra(SwipeActivity.VIEW_PROFILE_ID_KEY, personEntity.uid);

            ((Activity) context).startActivityForResult(viewProfileIntent, SwipeActivity.VIEW_PROFILE_REQUEST_CODE);
        });

        holder.numberOfPhotosTV.setText(String.valueOf(personEntity.imagesPaths.size()));
    }

    @Override
    public int getItemCount() {
        return profiles.size();
    }

    class PersonViewHolder extends RecyclerView.ViewHolder {
        public TextView cardName;
        public ImageView cardImage;
        public TextView numberOfPhotosTV;

        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);
            this.cardName = itemView.findViewById(R.id.cardName);
            this.cardImage = itemView.findViewById(R.id.cardImage);
            this.numberOfPhotosTV = itemView.findViewById(R.id.numberOfPhotosTV);
        }
    }
}

public class SwipeActivity extends AppCompatActivity implements CardStackListener {

    public static final String PROFILE_ID_EXTRA = "profile_id_extra";
    public static final String SWIPE_RESULT = "swipe_result";
    public static final String SWIPE_RESULT_JSON = "swipe_result_json";
    public static final String SWIPE_RESULT_TYPE = "swipe_result_type";
    public static final String VIEW_PROFILE_ID_KEY = "profile_id";
    public static final String VIEW_PROFILE_RESULT_KEY = "result";
    public static final int VIEW_PROFILE_REQUEST_CODE = 111;
    private PersonProfileCardAdapter cardInfoArrayAdapter;
    private CardStackView cardStackView;
    private CardStackLayoutManager manager;
    private PersonViewModel personViewModel;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe);
        context = this;

        manager = new CardStackLayoutManager(this, this);
        // manager.setStackFrom(StackFrom.Top);
        // manager.setVisibleCount(3);
        manager.setMaxDegree(20.0f);

        cardStackView = findViewById(R.id.card_stack_view);
        cardStackView.setLayoutManager(manager);

        cardInfoArrayAdapter = new PersonProfileCardAdapter(this, new ArrayList<>());
        personViewModel = new ViewModelProvider(this).get(PersonViewModel.class);
        personViewModel.getAllPersons().observe(this, persons -> {
            cardInfoArrayAdapter = new PersonProfileCardAdapter(this, persons);
            cardStackView.setAdapter(cardInfoArrayAdapter);
        });

        prepareButtons();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VIEW_PROFILE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                String returnedResult = "";
                if (bundle != null) {
                    returnedResult = bundle.getString(VIEW_PROFILE_RESULT_KEY, "");
                }

                if (returnedResult.equals(ResultType.Like.toString())) {
                    hitLike();
                } else if (returnedResult.equals(ResultType.Dislike.toString())) {
                    hitDislike();
                } else if (returnedResult.equals(ResultType.Superlike.toString())) {
                    hitSuperlike();
                }
            }
        }
    }

    public void refreshAllProfiles(View v) {
        cardStackView.setVisibility(View.VISIBLE);
        manager.setTopPosition(0);
    }

    private void hitDislike() {
        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(Direction.Left)
                .setDuration(Duration.Slow.duration)
                .build();
        manager.setSwipeAnimationSetting(setting);
        cardStackView.swipe();
    }

    private void hitLike() {
        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(Direction.Right)
                .setDuration(Duration.Slow.duration)
                .build();
        manager.setSwipeAnimationSetting(setting);
        cardStackView.swipe();
    }

    private void hitSuperlike() {
        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(Direction.Top)
                .setDuration(Duration.Slow.duration)
                .build();
        manager.setSwipeAnimationSetting(setting);
        cardStackView.swipe();
    }

    private void prepareButtons() {
        ImageButton dislikeButton = findViewById(R.id.dislikeButton);
        ImageButton likeButton = findViewById(R.id.likeButton);
        ImageButton superlikeButton = findViewById(R.id.superlikeButton);

        dislikeButton.setOnClickListener(v -> hitDislike());
        likeButton.setOnClickListener(v -> hitLike());
        superlikeButton.setOnClickListener(v -> hitSuperlike());
    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {

    }

    @Override
    public void onCardSwiped(Direction direction) {
        int topPosition = manager.getTopPosition();
        PersonEntity personEntity = cardInfoArrayAdapter.profiles.get(topPosition - 1);

        if (cardInfoArrayAdapter.profiles.size() == topPosition) {
            cardStackView.setVisibility(View.GONE);
        }

        String swipedResult = "";
        String swipeResultJSON = "";

        if (direction.toString().equals(Direction.Left.toString())) {
            swipedResult = ResultType.Dislike.toString();
            swipeResultJSON = personEntity.swipeLeftResultJSON;
        } else if (direction.toString().equals(Direction.Right.toString())) {
            swipedResult = ResultType.Like.toString();
            swipeResultJSON = personEntity.swipeRightResultJSON;
        } else if (direction.toString().equals(Direction.Top.toString())) {
            swipedResult = ResultType.Superlike.toString();
            swipeResultJSON = personEntity.swipeSuperResultJSON;
        }

        try {
            onCardExit(personEntity, swipedResult, swipeResultJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {

    }

    @Override
    public void onCardAppeared(View view, int position) {

    }

    @Override
    public void onCardDisappeared(View view, int position) {

    }

    private void onCardExit(PersonEntity personEntity, String swipeResult,
                            String swipeResultJSONString) throws JSONException {

        Intent swipeResultIntent;
        JSONObject jsonObject = new JSONObject(swipeResultJSONString);
        String detailedType = jsonObject.getString(Constants.SwipeResultActivityTypeDetailedStringKey);
        String swipeResultString = jsonObject.getString(Constants.SwipeResultActivityTypeSimpleStringKey);
        SwipeResultActivityTypeSimple swipeResultSimple = SwipeResultActivityTypeSimple.fromString(swipeResultString);

        switch (swipeResultSimple) {
            case Custom:
                swipeResultIntent = new Intent(context, CustomResultActivity.class);
                break;
            case Quiz:
                swipeResultIntent = new Intent(context, QuizResultActivity.class);
                break;
            case Nothing:
            default:
                return;
        }

        swipeResultIntent.putExtra(PROFILE_ID_EXTRA, personEntity.uid);
        swipeResultIntent.putExtra(SWIPE_RESULT, swipeResult);
        swipeResultIntent.putExtra(SWIPE_RESULT_JSON, swipeResultJSONString);
        swipeResultIntent.putExtra(SWIPE_RESULT_TYPE, detailedType);
        Log.d("swipe result", swipeResultJSONString);
        startActivity(swipeResultIntent);
    }
}
