package com.example.pickupswipe;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Constants {

    public static final String AnswerArrayString = "answerarray";
    public static final String YouString = "you";
    public static final String ThemString = "them";
    public static final String ItemTypeKey = "itemtype";

    public static final String InteractionTypeKey = "type";
    public static final String ActionTextKey = "text";
    public static final String PhotoPathKey = "path";

    public static final String SwipeResultActivityTypeDetailedStringKey = "resulttypedetailed";
    public static final String SwipeResultActivityTypeSimpleStringKey = "resulttypesimple";

    public static final String SwipeResultActionsArrayKey = "resultactions";

    public static final String QuizQuestionKey = "quizquestion";
    public static final String QuizAnswersKey = "quizanswers";
    public static final String QuizAnAnswerKey = "answer";
    public static final String QuizIsCorrectKey = "iscorrect";

    public static String datetimeToString(long datetime) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdf.setTimeZone(tz);
        return sdf.format(new Date(datetime));
    }
}
